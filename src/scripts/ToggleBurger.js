const burger = document.querySelector('.header__burger');
const closeBtn = document.querySelector('.header__close');
const menu = document.querySelector('.header__menu');
burger.addEventListener('click', function () {
  menu.classList.add('header__menu_mobile');
  closeBtn.classList.add('header__close_active');
});

closeBtn.addEventListener('click', function () {
  menu.classList.remove('header__menu_mobile');
  closeBtn.classList.remove('header__close_active');
});
